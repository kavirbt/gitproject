We have decided to design and implement a motor driver with positional feedback. 
An example of a motor driver with positional feedback would be a robotic arm lifting undefined weights to a specific position, this function would require the microcontroller to have feedback for the motors to accurate. 
From this we have determined that out microHAT would require a PiHAT to provide information on the external systems position.
A PiHat is a programmble devive that allows for extra functionality on top of the pi zero. 
Our Pi-Hat is a motor hat that will create PWM signals driving the motor.

Instructions for usage
-power on device 
-set setpoint using dials 
-Press start button 

Work split up 
Tefo - code
Raees - control and feedback 
Kavir - elctronics 

Bill of materials 
-Battery 12v 
-Stepper motor
-Pi-Zero 
-Pi-Hat
-Electronics 
